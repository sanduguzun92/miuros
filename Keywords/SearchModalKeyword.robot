*** Settings ***
Library  Collections

Resource    C:/Workspace/Miuros/Resources/SearchModalVar.robot


*** Keywords ***
Open_home_page_search
  [Documentation]  Open Home page and check search laptop filters are displayed properly

    open browser   ${Noteb_home_URL}  ${Chrome}
    Maximize Browser Window

    Element Should Be Visible    xpath://*[@id="SearchParameters"]

    Element Should Be Visible   xpath://*[@id="s_search_btn"]
    Element Should Be Visible   xpath://*[@id="s_search"]/span[2]/div/button
    Element Should Be Visible   xpath://*[@id="s_search"]/span[3]/div/button
    Element Should Be Visible   xpath://*[@id="s_mem"]
    Element Should Be Visible   xpath://*[@id="checkboxpre"]
    Element Should Be Visible   xpath://*[@id="s_hdd"]
    Element Should Be Visible   xpath://*[@id="s_dispsize"]
    Element Should Be Visible   xpath://*[@id="s_search"]/span[4]/div/button
    Element Should Be Visible   xpath://*[@id="s_search"]/span[5]/div/button
    Element Should Be Visible   xpath://*[@id="budget"]
    Element Should Be Visible   xpath://*[@id="s_search_btn"]
    Element Should Be Visible   xpath://*[@id="currency"]
    Element Should Be Visible   xpath://*[@id="sadvsearch"]
