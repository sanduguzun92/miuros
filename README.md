# Test Automation for Noteb.com
## Project overview

E-commerce site selling Laptops. Blablabla :D

## Tools used for testing
* Robot Framework used mainly for UI and API automation testing.


## Install Robot Framework on your Machine

1. Install Python https://www.python.org/downloads/,
2. Open CMD,
3. Check **Python** version "python --version" and **PIP** version "pip -- version" if needed updated them,
4. Install **Robot Framework** "pip install robotframework",
5. Run "pip show robotframework" details and version should be displayed,
6. Install library "pip install -U robotframework-jsonlibrary",
7. Install library "pip install robotframework-seleniumlibrary",
8. Install library "pip install robotframework-selenium2library",
9. Run "pip list" and make sure you have:
    * robot framework
    * requests
    * robotframework-requests
    * jsonpath_rw
    * jsonpath_rw_ext
    * robotframework-jsonlibrary
10. Install Any IDE that you prefer. Just make sure to set it up properly for the project by adding the previously installed packages.

**Note**: To make your life easy use IDE Extensions for Robot Framework.

## Git processes and rules
TBD
## Jenkins
TBD
