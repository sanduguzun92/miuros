*** Settings ***
Library  SeleniumLibrary

Resource   C:/Workspace/Miuros/Keywords/SearchModalKeyword.robot
Resource   C:/Workspace/Miuros/Keywords/ClearFieldOfChar.robot
Resource   C:/Workspace/Miuros/Resources/SearchModalVar.robot
Resource   C:/Workspace/Miuros/PageObject/HomePage/PO_HomePage.robot


*** Variables ***
${ama_min}=  5500
${ama_max}=  8400
${lowspec_min}=  100
${lowspec_max}=  400
${pre_ram_min}=  8
${pre_ram_max}=  16
${aft_ram_min}=  4
${aft_ram_max}=  16
${usd}=  $


*** Test Case ***
TC02 - Check_budget_imputs_accepted(Automation Candidate)
  [Documentation]  Checking if the Amount From and Amount To accept values in text box and Amount sliders adjust with the values provided

  Open_home_page_search

  ${field text}=  Get value  ${field1}
  ${field text length}=    Get Length    ${field text}
  Clear Field of Characters    ${field1}    ${field text length}
  Input text  xpath://input[@id='bdgmin']   ${ama_min}

  Click Button  xpath://*[@id="s_search"]/div[12]/div/div/div[2]/span/div/button

  ${field text}=  Get value  ${field2}
  ${field text length}=    Get Length    ${field text}
  Clear Field of Characters    ${field2}    ${field text length}
  Input text  xpath://input[@id='bdgmax']   ${ama_max}
  Press Keys  xpath://input[@id='bdgmax']  ENTER


  ${bdg_min}  Get Element Attribute  xpath://*[@id="budget"]/div/div[2]/div  aria-valuetext
  Should Be Equal As Numbers   ${bdg_min}	${ama_min}
  ${bdg_max}  Get Element Attribute  xpath://*[@id="budget"]/div/div[3]/div  aria-valuetext
  Should Be Equal As Numbers   ${bdg_max}	${ama_max}
  ${usd_curr}  Get Text  xpath://*[@id="s_search"]/div[12]/div/div/div[2]/span/div/button
  Should Be Equal  ${usd_curr}	  ${usd}

  Close browser


TC04 - Check_budget_change_impact_on_SearchFilter(Automation Candidate)

  Open_home_page_search

  Checkbox Should Be Selected   xpath://*[@id="checkboxpre"]
  ${ram_min}  Get Element Attribute  xpath://*[@id="s_mem"]/div/div[2]/div  aria-valuetext
  Should Be Equal As Numbers   ${ram_min}	${pre_ram_min}
  ${ram_max}  Get Element Attribute  xpath://*[@id="s_mem"]/div/div[3]/div  aria-valuetext
  Should Be Equal As Numbers   ${ram_max}	${pre_ram_max}



  ${field text}=  Get value  ${field1}
  ${field text length}=    Get Length    ${field text}
  Clear Field of Characters    ${field1}    ${field text length}
  Input text  xpath://input[@id='bdgmin']   ${lowspec_min}

  ${field text}=  Get value  ${field2}
  ${field text length}=    Get Length    ${field text}
  Clear Field of Characters    ${field2}    ${field text length}
  Input text  xpath://input[@id='bdgmax']   ${lowspec_max}
  Press Keys  xpath://input[@id='bdgmax']  ENTER

  Checkbox Should Not Be Selected    xpath://*[@id="checkboxpre"]
  ${ram_min}  Get Element Attribute  xpath://*[@id="s_mem"]/div/div[2]/div  aria-valuetext
  Should Be Equal As Numbers   ${ram_min}	${aft_ram_min}
  ${ram_max}  Get Element Attribute  xpath://*[@id="s_mem"]/div/div[3]/div  aria-valuetext
  Should Be Equal As Numbers   ${ram_max}	${aft_ram_max}

  Close browser


